export default {
  items: [
    {
      name: 'Dashboard',
      url: '/crm/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    }
  ]
};
